const imagesWrapper = document.querySelector('.images-wrapper');
const btnsWrapper = document.querySelector('.btn-wrapper');
imagesWrapper.firstElementChild.classList.add("active");

const btnStopShowing = document.createElement('button');
const btnResumeShowing = document.createElement('button');
btnStopShowing.innerText = 'Прекратить';
btnResumeShowing.innerText = 'Возобновить показ';
btnResumeShowing.style.display = 'none';
btnsWrapper.append(btnStopShowing, btnResumeShowing);

function showNextSlide(){
  const currentImage = imagesWrapper.querySelector('.active');
  currentImage.classList.remove("active");
  if (currentImage.nextElementSibling)
    currentImage.nextElementSibling.classList.add("active");
  else
    currentImage.parentElement.firstElementChild.classList.add("active");
}

const showingTime = 3000;
let playSlider = setInterval(showNextSlide, showingTime);

btnStopShowing.onclick = () => {
  btnResumeShowing.style.display = 'block';
  clearInterval (playSlider);
}

btnResumeShowing.onclick = () => {
  btnResumeShowing.style.display = 'none';
  playSlider = setInterval(showNextSlide, showingTime);
}
